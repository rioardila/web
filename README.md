# Server information #

**Web server:** nginx/1.4.6 (Ubuntu)

**Folder for html files:** /usr/share/nginx/html

**Technology used:** HTML5, CSS3

# How to restart nginx server #


```
service nginx restart
```

Other options:


```
#!

service nginx start/stop/status/restart
```


# Useful links #

[Trendy web color palettes](http://www.awwwards.com/trendy-web-color-palettes-and-material-design-color-schemes-tools.html)
